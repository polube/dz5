const express = require('express');
const UserService = require("../services/user-service");


class UserController {

    constructor(app) {

        const userRouter = express.Router();
        const users = new UserService();

        userRouter.get("/all", async (req, res) => {
            res.json(await users.getAllUsers())
        });

        userRouter.post("/", async (req, res) => {
            res.json(await users.setUser(req.body));
        });

        userRouter.delete('/:name', async (req, res) => {
            // console.log(req.params.name)
            res.json(await users.deleteUserByName(req.params));

        })


        app.use("/user", userRouter)
    }

}

module.exports = UserController;