const MongoService = require("./mongo.service");

class UserService {

    constructor() {
        this.userTable = MongoService.collections.users;
        if (!this.userTable) {
            throw new Error("Collection user does not exist!");
        };
    };

    async getAllUsers() {
        return await this.userTable.find({}).toArray();
    }

    async setUser(users) {
        return await this.userTable.insertOne(
            {
                name: users.name,
                surname: users.surname,
                age: users.age,
                isAdmin: false
            }
        );
    }


    async deleteUserByName(users) {
        return await this.userTable.deleteOne({ name: users.name });
    }

}




module.exports = UserService;